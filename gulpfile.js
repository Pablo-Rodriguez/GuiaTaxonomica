var gulp = require('gulp'),
	vulcanize = require('gulp-vulcanize'),
	browserify = require('browserify'),
	babelify = require('babelify'),
	buffer = require('vinyl-buffer'),
	source = require('vinyl-source-stream'),
	stylus = require('gulp-stylus'),
	nib = require('nib'),
	minifyHtml = require('gulp-minify-html'),
	imagemin = require('gulp-imagemin');

function browserifyOrigin (fileName) {
	browserify({
		entries: './client/js/' + fileName,
		debug: true,
		transform: [babelify]
	}).bundle()
		.pipe(source(fileName))
		.pipe(buffer())
		// .pipe(uglify())
		.pipe(gulp.dest('./app/public/js/'));
}

function globalCSS (fileName) {
	return gulp.src('client/stylus/' + fileName)
		.pipe(stylus({
			compress: true,
			use: [nib()]
		}))
		.pipe(gulp.dest('app/public/css/'));
}

var JSs = ['xhr.js'];

gulp.task('components', function () {
	return gulp.src('client/components/*.html')
		.pipe(vulcanize({
			inlineScripts: true,
			inlineCss: true
		}))
		.pipe(minifyHtml())
		.pipe(gulp.dest('app/public/components/'));
});

gulp.task('components:s', ['stylus'], function () {
	return gulp.src('client/components/*.html')
		.pipe(vulcanize({
			inlineScripts: true,
			inlineCss: true
		}))
		.pipe(minifyHtml())
		.pipe(gulp.dest('app/public/components/'));
});

gulp.task('js', function () {
	JSs.forEach(function (js) {
		browserifyOrigin(js);
	});
});

CSSs = ['add.styl', 'home.styl', 'search.styl', 'especieView.styl', 'error.styl', 'base.styl']

gulp.task('css', function () {
	CSSs.forEach(function (file) {
		globalCSS(file);
	});
});

gulp.task('stylus', function () {
	return gulp.src('client/stylus/*.styl')
		.pipe(stylus({
			compress: true,
			use: [nib()]
		}))
		.pipe(gulp.dest('client/css/'));
});

gulp.task('img', function () {
    return gulp.src('./client/img/**/*.*')
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('./app/public/img'));
});

gulp.task('fonts', function () {
	return gulp.src('client/fonts/**/*.*')
		.pipe(gulp.dest('app/public/fonts/'));
});

gulp.task('watch', function () {
	gulp.watch('client/js/**/*.js', ['js']);
	gulp.watch('client/stylus/**/*.styl', ['components:s']);
	gulp.watch('client/components/**/*.html', ['components']);
});

gulp.task('default', ['js', 'css', 'stylus', 'components', 'watch']);