import config from './app/config.js';
import http from 'http'
import mongoose from 'mongoose';
import ExpressServer from './app/expressServer.js';
let port = process.env.PORT || config.port;
let app = new ExpressServer();
let server = http.createServer(app.expressServer);

let dbUri = process.env.MONGOLAB_URI || config.db.dev;

mongoose.connect(dbUri, (err) => {
	if(err){
		console.log("ERROR: " + err);
	}else{
		console.log("Conectado á BD con exito");
	}
});

server.listen(port, () => {
	console.log("Magic on port " + port);
});