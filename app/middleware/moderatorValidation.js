
import {canPass} from '../util/index.js';

const not = {
    'get': ['/especies/moderar', '/especies/sin-moderar'],
    'post': ['/especies/moderar'],
    'put': [],
    'delete': []
};

export default function (config) {
    config = config || {};
    return function (req, res, next) {
        if(canPass(req, not)){
            next();
        }else{
            var user = req.decoded;
            if(user){
                if(user.moderator){
                    next();
                }else{
                    res.redirect('/');
                }
            }else{
                res.redirect('/usuarios/login');
            }
        }
    }
}