
import jwt from 'jsonwebtoken';

import config from '../config.js';
import priv from '../../private/private.js'
import {canPass} from '../util/index.js'

const not = {
    'get': ['/especies/nueva', '/especies/moderar', '/especies/sin-moderar', '/especies/reset', '/especies/erase/*', '/usuarios', '/usuarios/remove/*'],
    'post': ['/especies/add', '/especies/moderar'],
    'put': [],
    'delete': []
};

export default function (config) {
    config = config || {};
    return function (req, res, next) {
        if(canPass(req, not)){
            next();
        }else{
            let token = req.cookies.interpeixeToken || req.body.token;
            if(token){
                let secret = process.env.JWT_SECRET || priv.jwt || conf.jwt;
                jwt.verify(token, secret, (err, decoded) => {
                    if(err){
                        res.redirect('/usuarios/login');
                    }else{
                        req.decoded = decoded;
                        next();
                    }
                });
            }else{
                res.redirect('/usuarios/login');
            }
        }
    }
}