let canPass = function (req, not) {
    let method = req.method.toLowerCase();
    let url = req.url;
    if(not[method].indexOf(url) !== -1){
        return false;
    }else{
        url = url.split('/');
        url[url.length - 1] = '*';
        url = url.join('/');
        if(not[method].indexOf(url) !== -1){
            return false;
        }
    }
    return true;
};

export {canPass};