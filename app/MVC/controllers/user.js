
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

import config from '../../config.js';
import priv from '../../../private/private.js';

import Model from '../models/user.js';
import View from '../views/user.js';

let User = function (config) {
    config = config || {};

    this.model = new Model();
    this.view = new View();

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    };

    this.serverError = (res) => {
        let msg = "Hemos tenido un problemilla :( no es culpa tuya";
        let err = "Error 500";
        this.view.error(res, {msg, err});
    };

    this.responseMsg = (res, success, info, msg) => {
        res.json({
            success,
            info,
            msg
        });
    };
};

User.prototype.get_root = function(req, res) {
    this.model.all().exec((err, users) => {
        if(err){
            this.serverError(res);
        }else{
            this.view.all(res, {users: JSON.stringify(users)});
        }
    });
};

User.prototype.get_signup = function(req, res) {
    this.view.nuevo(res);
};

User.prototype.get_login = function(req, res) {
    this.view.login(res);
};

User.prototype.post_nuevo = function(req, res) {
    let model = req.body;
    res.setHeader("Content-type", "application/json");
    
    //Validaciones
    if(!model.name){
        res.json({
            success: false,
            msg: "Debe introducir un nombre de usuario"
        });
    }

    if(model.password.length < 8){
        res.json({
            success: false,
            msg: "La contraseña debe tener como mínimo 8 caracteres"
        });
    }

    if(model.password !== model.second){
        res.json({
            success: false,
            msg: "Los campos de contraseña no coinciden"
        });
    }

    //Inscripcion en la base de datos
    this.model.new(model)
        .then(() => {
            res.json({
                success: true,
                msg: "Enhorabuena, ya eres usuario de Interpeixe :)"
            });
        })
        .catch((err) => {
            if(err.code === 11000) {
                res.json({
                    success: false,
                    msg: "Ese nombre de usuario no está disponible :("
                });
            }else{
                res.json({
                    success: false,
                    msg: "Hemos tenido un problemilla"
                });
            }
        });
};

User.prototype.post_login = function(req, res) {
    res.setHeader("Content-type", "application/json");
    this.model.one(req.body.name.toLowerCase()).exec((err, user) => {
        if(err){
            this.serverError(res);
        }else if(!user){
            this.responseMsg(res, true, false, "El usuario no existe");
        }else{
            if(bcrypt.compareSync(req.body.password, user.password)){

                delete user.password;
                let secret = process.env.JWT_SECRET || priv.jwt || config.jwt;
                let token = jwt.sign(user, secret, {expiresInMinutes: 1440});

                res.cookie('interpeixeToken', token);
                this.responseMsg(res, true, true, "Listo :)");
            }else{
                this.responseMsg(res, true, false, "Contraseña incorrecta");
            }
        }
    });
};

User.prototype.get_remove_data = function(req, res) {
    if(req.decoded.admin === false){
        res.redirect('/');
    }
    res.setHeader("Content-type", "text/plain")
    this.model.remove(req.params.data.toLowerCase()).then(() => {
        res.end('Todo bien');
    })
    .catch((err) => {
        if(err.mycode === 1){
            res.end('No existe ese usuario');
        }
        res.end('Hemos tenido un problemilla');
    });
};

export default User;