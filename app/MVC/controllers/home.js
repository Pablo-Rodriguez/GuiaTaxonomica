//Controlador do home

import jwt from 'jsonwebtoken';
import priv from '../../../private/private.js';
import conf from '../../config.js';

import View from '../views/home.js';

let Home = function (config) {
	config = config || {};

	this.view = new View();

	this.response = (conf) => {
		this[conf.resource](conf.req, conf.res);
	}
};

//Home
Home.prototype.get_root = function (req, res) {
    let obj = {};
    let token = req.cookies.interpeixeToken || req.body.token;
    if(token){
        let secret = process.env.JWT_SECRET || priv.jwt || conf.jwt;
        jwt.verify(token, secret, (err, decoded) => {
            if(!err && decoded){
                obj.user = decoded;
            }
            this.view.root(res, obj);
        });
    }else{
        this.view.root(res, obj);
    }
};

export default Home;