//Controlador de Especie
import fs from 'fs';

import Model from '../models/especie.js';
import View from '../views/especie.js';

let Especie = function (config) {
	config = config || {};
	
	this.view = new View();
	this.model = new Model();

	this.limit = 5;

	this.response = (conf) => {
		this[conf.resource](conf.req, conf.res);
	}
};

//Error comun que se devolve ao cliente en moitas ocasions
Especie.prototype.commonError = function (res, msg) {
	msg = msg || "Hemos tenido un problemilla :(";
	res.json({
		success: false,
		msg: msg
	});
};

Especie.prototype.spaces = function(name) {
	name = name.split('-');
	name = name.join(' ');
	return name;
};

//Analizador dos strings de tags que parsea estes para convertilos en arrays
Especie.prototype.parse_tags = function (tags) {
	tags = tags.split(',');
	for(let i in tags){
		tags[i] = tags[i].trim();
		tags[i] = tags[i].toLowerCase();
	}
	let output = [];
	for(let tag of tags){
		if(output.indexOf(tag) === -1 && tag !== ''){
			output.push(tag);
		}
	}
	return output;
};

//Devolve a vista de engadir unha especie
Especie.prototype.get_nueva = function (req, res) {
	this.view.add(res);
};

//Devolve a vista de busqueda
Especie.prototype.get_buscar = function (req, res) {
	this.view.search(res);
};

//Template de moderacion de especies
Especie.prototype.get_moderar = function(req, res) {
	this.view.notModerated(res);
};

//Parte de moderacion de especies
Especie.prototype['get_sin-moderar'] = function(req, res) {
	this.model.notModerated().exec((err, especies) => {
		if(err){
			console.log(err);
		}else{
			res.setHeader("Content-type", "application/json");
			res.json({
				success: true,
				data: especies
			});
		}
	});
};

//Permite cambiar o atributo moderated dunha especie a true
Especie.prototype.post_moderar = function(req, res) {
	new Promise((resolve, reject) => {
		this.model.byId(req.body.id).exec((err, especie) => {
			if(err){
				console.log(err);
				reject(err);
			}else if(!especie){
				let err = {mycode: 1};
				reject(err);
			}else{
				resolve(especie);
			}
		});
	})
		.then((especie) => {
			especie.moderated = true;
			especie.moderatedBy = req.decoded.name;
			return new Promise((resolve, reject) => {
				especie.save((err) => {
					if(err){
						reject(err);
					}else{
						resolve();
					}
				});
			});
		})
		.then(() => {
			res.setHeader("Content-type", "application/json")
			res.json({
				success: true
			});
		})
		.catch((err) => {
			res.setHeader("Content-type", "application/json");
			res.json({
				success: false
			})
		});
};

//Devolve a vista dunha especie concreta
Especie.prototype.get_ver_data = function (req, res) {
	let scientific = this.spaces(req.params.data);
	this.model.byScientific(scientific).exec((err, especie) => {
		if(err){
			console.log("ERROR: " + err);
			this.view.error(res, {
				err: 'Error 500',
				msg: 'Hemos tenido un problemilla :( no es culpa tuya'
			});
		}else if(!especie){
			this.view.error(res, {
				err: 'Error 404',
				msg: 'La especie que busca no está en nuestra base de datos :('
			});
		}else{
			let strEspecie = JSON.stringify(especie);
			let obj = {
				especie: strEspecie
			};
			if(obj.especie.img === ''){
				obj.especie.img = '/img/peixe.jpeg';
			}
			this.view.especie(res, obj);
		}
	});
};

//Devolve as ultimas especies engadidas
Especie.prototype.get_ultimas = function(req, res) {
	res.setHeader("Content-type", "application/json");
	this.model.last(6).exec((err, especies) => {
		if(err){
			res.json({
				success: false
			});
		}else{
			res.json({
				success: true,
				especies
			});
		}
	});
};

//Busca as especies que se corresponden cos tags e as devolve
Especie.prototype.post_search = function (req, res) {
	let tags = req.body.tags;
	if(!tags){
		res.setHeader("Content-type", "application/json");
		res.json({
			success: true,
			data: false
		});
	}
	tags = this.parse_tags(tags);
	let start = this.limit * req.body.page;
	//Realizar la busqueda en la base de datos
	this.model.byTags(tags, start).exec((err, especies) => {
		if(err){
			console.log("ERROR: " + err);
			res.setHeader("Content-type", "application/json");
			res.json({
				success: false,
				info: false
			});
		}else if(!especies || especies.length === 0){
			res.setHeader("Content-type", "application/json");
			res.json({
				success: true,
				info: false
			});
		}else{
			res.setHeader("Content-type", "application/json");
			let left = especies.length - this.limit >= 0 ? especies.length - this.limit : 0;
			res.json({
				success: true,
				info: true,
				data: especies.splice(0, this.limit),
				left
			});
		}
	});
};

//Controla a subida de especies
Especie.prototype.post_add = function (req, res) {
	req.body.img = req.files.img;
	req.body.user = req.decoded.name;
	req.body.tags = this.parse_tags(req.body.tags);
	res.setHeader("Content-type", "application/json");

	this.model.newEspecie(req.body)

	.then(() => {
		res.json({
			success: true,
			msg: "Todo salió bien :)"
		});
	})

	.catch((report) => {
		if(report.stored){
			this.model.removeById(report.id)
			.then(() => {
				if(report.converted){
					fs.unlink(report.path, (err) => {
						return;
					});
				}else{
					return;
				}
			})
			.then(() => {
				if(report.uploaded){
					this.model.erase(report.id);
					return;
				}else{
					return;
				}
			})
			.catch((err) => {
				console.log("ERROR: " + err);
			});
		}
		this.commonError(res, report.msg);
	});
};

//Resetear a base de datos e as imaxes durante o desenvolvemento
Especie.prototype.get_reset = function (req, res) {
	this.model.reset(res);
};

//Borra unha imaxe en concreto que se pasa como parametro de ruta
// /especies/erase/:id
Especie.prototype.get_erase_data = function(req, res) {
	this.model.erase(req.params.data).then(() => {
		res.end();
	});
};

export default Especie;