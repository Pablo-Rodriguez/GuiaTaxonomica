
let Defaults = function (config) {
    config = config || {};

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    }
};

Defaults.prototype.get_root = function (req, res) {
    res.render('error', {
        err: "Error 404",
        msg: "Lo que busca no está aquí :("
    });
};

export default Defaults;