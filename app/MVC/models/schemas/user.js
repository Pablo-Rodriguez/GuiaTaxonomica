
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let user = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        default: false
    },
    moderator: {
        type: Boolean,
        default: false
    }
});

export default mongoose.model('User', user);