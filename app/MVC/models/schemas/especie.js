//Schema de mongoose da Especie
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

//Modelado da especie
let especie = new Schema({
	name: String,
	scientific: {
		type: String,
		unique: true,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	img: {
		type: String
	},
	tags: [String],
	tagsLength: Number,
	date: {
		type: Date,
		default: Date.now
	},
	moderated: {
		type: Boolean,
		default: false
	},
	uploadedBy: String,
	moderatedBy: String
});

export default mongoose.model('Especie', especie);