
import bcrypt from 'bcryptjs';
import Schema from './schemas/user.js';

let User = function (config) {
    config = config || {};
}

User.prototype.all = function(skip) {
    skip = skip || 0;
    return Schema.find().select('name admin moderator').skip(skip);
};

User.prototype.one = function(name) {
    return Schema.findOne({name});
};

User.prototype.new = function(model) {
    let salt, hash;
    if(model.password){
        salt = bcrypt.genSaltSync(10);
        hash = bcrypt.hashSync(model.password, salt);
    }
    
    let user = new Schema({
        name: model.name.toLowerCase(),
        password: hash,
        admin: false,
        moderator: false
    });

    return new Promise((resolve, reject) => {
        user.save((err) => {
            if(err){
                reject(err);
            }else{
                resolve();
            }
        });
    });
};

User.prototype.remove = function(name) {
    return new Promise((resolve, reject) => {
        Schema.findOne({name}, (err, user) => {
            if(err){
                reject(err);
            }else if(!user){
                let err = {mycode: 1};
                reject(err);
            }else{
                resolve(user);
            }
        });
    })
    .then((user) => {
        user.remove((err) => {
            if(err){
                return Promise.reject(err);
            }else{
                return Promise.resolve();
            }
        });
    });
};

export default User;