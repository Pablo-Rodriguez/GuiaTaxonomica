//Modelo de Especie
import path from 'path';
import fs from 'fs';

import Imagemin from 'imagemin';
import cloudinary from 'cloudinary';

import Schema from './schemas/especie.js';

let priv;
if(!process.env.pro){
	priv = require('../../../private/private.js');
}

let Especie = function (config) {
	config = config || {};
	//Configurar cloudinary
	this.cloudinary = cloudinary;
	this.cloudinary.config({
		cloud_name: process.env.CLOUD_NAME || priv.cloudinary.name,
		api_key: process.env.CLOUD_KEY || priv.cloudinary.key,
		api_secret: process.env.CLOUD_SECRET || priv.cloudinary.secret
	});
};

//Busca por id e devolve un query para ser executado
Especie.prototype.byId = function (id) {
	return Schema.findById(id);
};

//Busca por nome cientifico e devolve un query para ser executado
Especie.prototype.byScientific = function(scientific) {
	return Schema.findOne({scientific});
};

//Busca por tags e devolve un query para ser ejecutado
Especie.prototype.byTags = function (tags, start) {
	return Schema.find().where('moderated').equals(true).where('tags').all(tags).skip(start).sort({tagsLength: -1});
};

//Devolve as especies non moderadas coa sua correspondente paxinacion
Especie.prototype.notModerated = function(page=0,limit=10) {
	return Schema.find().where('moderated').equals(false).skip(page).limit(limit).sort({date: 'ascending'});
};

//Ultimos modelos engadidos
Especie.prototype.last = function(limit=10) {
	return Schema.find().where('moderated').equals(true).limit(limit).sort({date: 'descending'});
};

//Crea unha nova especiena base de datos e sube a imaxe a cloudinary
Especie.prototype.newEspecie = function (model, res) {
	let especie = new Schema({
		name: model.name,
		scientific: model.scientific.toLowerCase(),
		description: model.description,
		tags: model.tags,
		tagsLength: model.tags.length,
		uploadedBy: model.user
	});
	let report = {
		msg: "Hemos tenido un problemilla :(",
		id: String(especie._id),
		path: model.img.path,
		stored: false,
		converted: false,
		uploaded: false
	};

	return new Promise((resolve, reject) => {
		especie.save((err) => {
			if(err){
				if(err.code === 11000){
					report.msg = "Esa especie ya está en nuestra base de datos :)";
				}
				reject(report);
			}else{
				report.stored = true;
				resolve();
			}
		});
	})
	.then(() => {
		let spl = model.img.path.split('/');
		let name = spl[spl.length - 1];
		let ext = name.split('.')[1].toLowerCase();
		let fn, opt;
		switch(ext){
			case 'jpg':
			case 'jpeg':
				fn = 'gifsicle';
				opt = {
					interlaced: true
				};
				break;
			case 'png':
				fn = 'jpegtran';
				opt = {
					progressive: true
				};
				break;
			case 'gif':
				fn = 'optipng';
				opt = {
					optimizationLevel: 3
				};
				break;
			default:
				report.msg = "El formato de la imagen no esta entre los permitidos";
				throw report;
		}
		let folder = path.join('/', 'tmp', 'build');
		let newPath = path.join(folder, name);

		return new Promise((resolve, reject) => {
			new Imagemin()
				.src(model.img.path)
				.dest(folder)
				.use(Imagemin[fn](opt))
				.run((err, files) => {
					if(err){
						reject(report);
					}else{
						report.converted = true;
						resolve(newPath);
					}
				});
		});
	})
	.then((newPath) => {
		return new Promise((resolve, reject) => {
			let stream = cloudinary.uploader.upload_stream((result) => {
				especie.img = result.secure_url;
				report.uploaded = true;
				return resolve(newPath);
			}, {public_id: String(especie._id)});
			let is = fs.createReadStream(newPath).pipe(stream);
		});
	})
	.then((newPath) => {
		fs.unlink(newPath, (err) => {
			//No se envia el error porque no es una causa de que
			//error que implique el no poder guardar la especie
			return;
		});
	})
	.then(() => {
		especie.save((err) => {
			if(err){
				if(err.code === 11000){
					report.msg = "Esa especie ya existe en nuestra base de datos :)";
				}
				throw report;
			}else{
				return;
			}
		});
	})
	.then(() => {
		fs.unlink(model.img.path, (err) => {
			//No se envia el error porque no es una causa de
			//error que implique el no poder guardar la especie
			return;
		});
	});
};

//Borra da base de datos unha especie buscandoa por id
Especie.prototype.removeById = function(id) {
	return new Promise((resolve, reject) => {
		Schema.findById(id, (err, especie) => {
			if(err){
				throw err;
			}else{
				resolve(especie);
			}
		});
	}).then((especie) => {
		especie.remove((err) => {
			if(err){
				throw err;
			}else{
				return;
			}
		});		
	});
};

//Resetea a base de datos e borra as imaxes de cloudinary
Especie.prototype.reset = function (res) {
	Schema.find((err, especies) => {
		if(err){
			console.log("ERROR: " + err);
		}else{
			let ids = [];

			especies.forEach((especie) => {
				ids.push(String(especie._id));
			});

			this.erase(ids)
			.then(() => {
				let promises = [];
				especies.forEach(function (especie) {
					promises.push(new Promise((resolve, reject) => {
						especie.remove((err) => {
							resolve();
						});
					}));
				});
				return Promise.all(promises);
			})
			.then(() => {
				res.end();
			});
		}
	});
};

//Borra unha imaxe ou varias de cloudinary en funcion do public_id
Especie.prototype.erase = function (ids) {
	return new Promise((resolve, reject) => {
		this.cloudinary.api.delete_resources(ids, (result) => {
			resolve();
		});
	})
}

export default Especie;