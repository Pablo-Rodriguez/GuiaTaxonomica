//Aqui estan os distintos controladores da aplicacion
import Home from './controllers/home.js';
import Especie from './controllers/especie.js';
import User from './controllers/user.js';
import Defaults from './controllers/defaults.js';
export default {
	home: Home,
	especies: Especie,
    usuarios: User,
    '*': Defaults
}