//Vista de Especie (engadir, buscar, especie, inexistente, erro)
let Especie = function (config) {
	config = config || {};
}

Especie.prototype.notModerated = function(res, obj={}) {
    res.render('notModerated', obj);
};

Especie.prototype.add = function (res, obj={}) {
	res.render('addEspecie', obj);
};

Especie.prototype.search = function (res, obj={}) {
	res.render('searchEspecie', obj);
};

Especie.prototype.especie  = function (res, obj={}) {
	res.render('especie', obj);
};

Especie.prototype.error = function (res, obj={}) {
	res.render('error', obj);
};

export default Especie;