
let User = function (config) {
    config = config || {};
}

User.prototype.error = function(res, obj) {
    obj = obj || {};
    res.render('error', obj);
};

User.prototype.all = function(res, obj) {
    obj = obj || {};
    res.render('users', obj);
};

User.prototype.nuevo = function(res, obj) {
    obj = obj || {};
    res.render('newUser', obj);
};

User.prototype.login = function(res, obj) {
    obj = obj || {};
    res.render('login', obj);
};

export default User;